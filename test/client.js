// WebSocket initialization
var SERVER_PORT = 8080;
var WebSocketServer = require('ws').Server;
var myWSS = new WebSocketServer({port: SERVER_PORT});
var myWSConnections = new Array;


// WebSocket handlers
function wsHandler(client)
{
    console.log("WS: New Connection... ");

    // Pushing connection to the connections pool.
    myWSConnections.push(client);

    // When client sends messages
    client.on('message', function (data)
    {
        console.log("WS: From client: "+data);
    });

    // When client closes connection
    client.on('close', function ()
    {
        console.log("WS: Connection close");
        var clientPosition = myWSConnections.indexOf(client);
        myWSConnections.splice(clientPosition, 1);
    });
}

// WebSocket events.
myWSS.on('connection', wsHandler);

