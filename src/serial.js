/**
 * Archivo: serial.js
 * autor: jaime aza 
 * fecha: 10 de mayo 2020
 * Descripción: envía a la base de datos los los valores leidos por el serial dependiendo 
 * de la petición del usuario. 
 */

// Serial port initialization
var SerialPort = require('serialport');
var Delimiter = require('@serialport/parser-delimiter');

var mySerial = new SerialPort('COM4', {
    baudRate: 9600, 
    autoOpen: false, 
    flowControl: false});


mySerial.on('open', function() {
    console.log('puerto serial abierto');
})

// Serial port sentences
mySerial.open();

// Database initialization
var mysql = require('mysql');
var myConnect = mysql.createConnection({
    host: 'localhost',
    user: 'jjat',
    password: 'jjatjjat',
    database: 'jjat'
});
// Database conection
myConnect.connect(function (error){
    if (error)
    {
        console.log('ERROR: Cannot connect to database: '+error.stack);
    }
    console.log('Connected as '+myConnect.captureDataId);
    console.log('Connection state: '+myConnect.state);
});


//variables de control
var idTipoProceso = 0;
var canalAnalogoSeleccionado = 0;
var canalDigitalSeleccionado = 0;
var canalSalidaSeleccionado = 0;
var estadoSalidaSeleccionado = 0;
var tiempoMuestroSeleccionado = 900;
//variables de datos de envios
var valorDato = 0;
var tiempo = 0;    
var counter = 0;

var myParserSP = mySerial.pipe(new Delimiter({delimiter: '~~'}));
myParserSP.on('data', function(data) {
    var myQuery = "select int_proceso_id, parametro1, parametro2, parametro3, parametro4 from int_proceso_control where id=(select max(id) from int_proceso_control)";
    myConnect.query(myQuery, function (error, results, fields){
        if (error) throw error;
        idTipoProceso = results[0].int_proceso_id
        canalAnalogoSeleccionado = results[0].parametro1;
        canalDigitalSeleccionado = results[0].parametro2;
        canalSalidaSeleccionado =  results[0].parametro3;
        estadoSalidaSeleccionado = results[0].parametro4;
    });
    console.log("int_proceso_id: "+idTipoProceso)
    console.log("canalAnalogoSeleccionado: "+canalAnalogoSeleccionado)
    console.log("canalDigitalSeleccionado: "+canalDigitalSeleccionado)
    console.log("canalSalidaSeleccionado: "+canalSalidaSeleccionado)
    console.log("estadoSalidaSeleccionado: "+estadoSalidaSeleccionado)

    var myQuery = "select tiempo_muestreo from int_proceso where id=(select max(id) from int_proceso)";
    myConnect.query(myQuery, function (error, results, fields){
        if (error) throw error;
        tiempoMuestroSeleccionado = results[0].tiempo_muestreo;
    });
    console.log("tiempoMuestroSeleccionado: "+tiempoMuestroSeleccionado)
    mySerial.write("~~"+canalAnalogoSeleccionado+canalDigitalSeleccionado+canalSalidaSeleccionado+estadoSalidaSeleccionado+tiempoMuestroSeleccionado, (err) => {
        if (err) {
        return console.log('Error on write: ', err.message);
        }
    });

    if (idTipoProceso == 1) {
        valorDato = (data.toString().substring(0,1))*5.0;        
    }
    else if (idTipoProceso == 2){
        valorDato = (data.toString().substring(1,5))*(5.0/1023);
    }
    console.log("valorDato: "+ valorDato)
    console.log("tiempo: "+counter*(tiempoMuestroSeleccionado*0.001))  

    if (counter == 10) {
        myParserSP.end();
    }

    tiempo = counter*(tiempoMuestroSeleccionado*0.001);
    var myQuery = "insert into int_proceso_vars_data set valor='" + valorDato + "', tiempo='" + tiempo+ "', fecha=CURDATE() , hora= CURTIME(), int_proceso_vars_id=" + 9;
    myConnect.query(myQuery, function(error, result){
        if (error) throw error;
        console.log('Affected rows: '+result.affectedRows);
        console.log('Insert ID: '+result.insertId);
    });
    counter++;    
})

mySerial.on('err', function(err) {
    console.log(err.message)
})