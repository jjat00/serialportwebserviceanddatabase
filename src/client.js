/**
 * Archivo: client.js
 * autor: jaime aza 
 * fecha: 10 de mayo 2020
 * Descripción: envía a la base de datos los variables de control dependiendo del cliente.
 */

// WebSocket initialization
var SERVER_PORT = 8080;
var WebSocketServer = require('ws').Server;
var myWSS = new WebSocketServer({port: SERVER_PORT});
var myWSConnections = new Array;

// Connection object.
var mysql = require('mysql');
var myConnect = mysql.createConnection({
    host: 'localhost',
    user: 'jjat',
    password: 'jjatjjat',
    database: 'jjat'
});
// Database conection
myConnect.connect(function (error){
    if (error)
    {
        console.log('ERROR: Cannot connect to database: '+error.stack);
    }
    console.log('Connected as '+myConnect.captureDataId);
    console.log('Connection state: '+myConnect.state);
});

//variables datos de envio
var nombreSenal ="ADC0";
var int_proceso_tipo_id = 1;
var tiempo_muestreo = 900;
var descripcion = "parametros senales";
var senal = "senalAnalogaCanal0";
var int_proceso_id = 0;
var canalSenalAnolaga = 0;
var canalSenalDigital = 0;
var canalSalidaDigital = 0;
var estadoSenalSalida = 0;
var ultimoIdProcesoVarsData = 10000;
var flagEstadoSenalSalida0 = 0;
var flagEstadoSenalSalida1 = 0;

// WebSocket handlers
function wsHandler(client)
{
    console.log("WS: New Connection... ");

    // Pushing connection to the connections pool.
    myWSConnections.push(client);

    // When client sends messages
    client.on('message', function (data)
    {
        console.log("WS: From client: "+data);
        senal=data;
       
        switch (senal)
        {
            case "senalAnalogaCanal0":
                nombreSenal = "ADC0";
                int_proceso_tipo_id=2;
                canalSenalAnolaga = 0;
                tiempo_muestreo = 100;
                estadoSenalSalida = 0;
                descripcion = "parametros senales analogas";
                break;
            case "senalAnalogaCanal1":
                nombreSenal = "ADC1";
                int_proceso_tipo_id=2;
                canalSenalAnolaga = 1;
                tiempo_muestreo = 200;
                estadoSenalSalida = 0;
                descripcion = "parametros senales analogas";
                break;
            case "senalAnalogaCanal2":
                nombreSenal= "ADC2";
                int_proceso_tipo_id=2;
                canalSenalAnolaga = 2;
                tiempo_muestreo = 300;
                estadoSenalSalida = 0;
                descripcion = "parametros senales analogas";
                break;
            case "senalAnalogaCanal3":
                nombreSenal = "ADC3";
                int_proceso_tipo_id=2;
                canalSenalAnolaga = 3;
                tiempo_muestreo = 400;
                estadoSenalSalida = 0;
                descripcion = "parametros senales analogas";
                break;
            case "senalDigitalCanal0":
                nombreSenal = "DIG0";
                int_proceso_tipo_id=1;
                canalSenalDigital = 0;
                estadoSenalSalida = 0;
                tiempo_muestreo = 900;
                descripcion = "parametros senales analogas";
                break;
            case "senalDigitalCanal1":
                nombreSenal = "DIG1";
                int_proceso_tipo_id=1;
                canalSenalDigital = 1;
                estadoSenalSalida = 0;
                tiempo_muestreo = 900;
                descripcion = "parametros senales digitales";
                break;
            case "salidaDigital0":
                nombreSenal = "salidaDigital0";
                int_proceso_tipo_id=3;
                canalSalidaDigital = 0;
                if (flagEstadoSenalSalida0 == 0) {
                    estadoSenalSalida = 1;
                    flagEstadoSenalSalida0 = 1                    
                    console.log("[1]")
                }
                else {
                    estadoSenalSalida = 0;
                    flagEstadoSenalSalida0 = 0              
                    console.log("[2]")
                }
                descripcion = "parametros senales de salida";
                break;
            case "salidaDigital1":
                nombreSenal= "salidaDigital1";
                int_proceso_tipo_id=3;
                canalSalidaDigital = 1;
                if (flagEstadoSenalSalida1 == 0) {
                    estadoSenalSalida = 1;
                    flagEstadoSenalSalida1 = 1                    
                }
                else {
                    estadoSenalSalida = 0;
                    flagEstadoSenalSalida1 = 0                   
                }
                descripcion = "parametros senales de salida";
                break;
        }

        // Inserting data into int_proceso
        var myQuery = "insert into int_proceso set nombre='" + nombreSenal + "', int_proceso_tipo_id='" + int_proceso_tipo_id + "', tiempo_muestreo=" + tiempo_muestreo;
        myConnect.query(myQuery, function(error, result){
            if (error) throw error;
            console.log('Affected rows: '+result.affectedRows);
            console.log('Insert ID: '+result.insertId);
            int_proceso_id = result.insertId;
        });
        
        // Inserting data into int_proceso_control
        var myQuery = "insert into int_proceso_control set nombre='" + nombreSenal + "', descripcion='" + descripcion+ "', int_proceso_id='" + int_proceso_tipo_id+"', parametro1='" +canalSenalAnolaga+"', parametro2='" +canalSenalDigital+"', parametro3='" +canalSalidaDigital+"', parametro4=" +estadoSenalSalida;
        myConnect.query(myQuery, function(error, result){
            if (error) throw error;
            console.log('Affected rows: '+result.affectedRows);
            console.log('Insert ID: '+result.insertId);
        });
        
    });

    // When client closes connection
    client.on('close', function ()
    {
        console.log("WS: Connection close");
        var clientPosition = myWSConnections.indexOf(client);
        myWSConnections.splice(clientPosition, 1);
    });
}

// WebSocket events.
myWSS.on('connection', wsHandler);

function enviarDatosWs(){
    setTimeout(function() {
        if (myWSConnections.length > 0){
            for (wsConn in myWSConnections){
                var myQuery = "select valor, tiempo, id from int_proceso_vars_data where id=(select max(id) from int_proceso_vars_data)";
                myConnect.query(myQuery, function (error, results){
                    if (error) throw error;
                    if (ultimoIdProcesoVarsData < results[0].id) {
                        ultimoIdProcesoVarsData = results[0].id;
                        myWSConnections[wsConn].send(results[0].valor+","+results[0].tiempo);
                    }else{
                        ultimoIdProcesoVarsData = results[0].id;
                    }

                });
            }
        }
        enviarDatosWs();
    }, 100);   
};
enviarDatosWs()
