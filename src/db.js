var mysql = require('mysql');

// Connection object.
var myConnect = mysql.createConnection({
    host: 'localhost',
    user: 'jjat',
    password: 'jjatjjat',
    database: 'jjat'
});

// Connecting...
myConnect.connect(function (error){
    if (error)
    {
        console.log('ERROR: Cannot connect to database: '+error.stack);
    }

    console.log('Connected as '+myConnect.threadId);
    console.log('Connection state: '+myConnect.state);

    // Inserting data into int_usuarios_tipo
    //var myQuery = "insert into int_usuarios_tipo set nombre='NodeJS', descripcion='Insert from NodeJS'";
    //myConnect.query(myQuery, function(error, result){
    //    if (error) throw error;
    //
    //    console.log('Affected rows: '+result.affectedRows);
    //    console.log('Insert ID: '+result.insertId);
    //});

    // Selecting data.
    var myQuery = "select id, nombres, apellidos, email from int_usuarios order by nombres, apellidos ASC";
    myConnect.query(myQuery, function (error, results, fields){
        if (error) throw error;

        console.log('Selected data: ');
        console.log(results);

        console.log('*******************');
        console.log('Specific data: ');
        console.log('User id '+results[0].id+' User name: '+results[0].nombres+' '+results[0].apellidos);

        console.log('*******************');
        console.log('Fields: ');
        console.log(fields);
    });
});

// End
console.log('End Program...')